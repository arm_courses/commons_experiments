# Git-It

## Basic Information

1. 学时：2-4
1. 目的：
    1. 掌握`git`的安装
    2. 掌握`git`的基本使用
2. 内容与要求：通过`git-it`在本地安装和练习`git`和`github`
3. 环境条件：
    1. 软硬系统环境：`Desktop`(`macOS`或`Windows`或`Linux`)或`Server`(`Linux`)
    2. 应用软件环境：`browser`(建议`chrome`/`firefox`/`safari`), `git`

## Guides

>Git-it is a (Mac, Win, Linux) Desktop App for Learning Git and GitHub
>>[github: jlord/git-it-electron](https://github.com/jlord/git-it-electron)

1. 下载`git-it`：<https://github.com/jlord/git-it-electron/releases>
2. 下载`git`：<https://git-scm.com/downloads>
3. 运行`git-it`并按其中的指引练习`git`和`github`
    - `Windows`下，解压下载的压缩包，运行文件夹中`Git-it.exe`即可运行

![git-it](./.assets/image/git-it.png)

## Requirements of Report

1. 完成各个关卡
2. 将各个关卡的练习结果截图保存到实验报告中

## References

1. [廖雪峰. Git教程.](https://www.liaoxuefeng.com/wiki/896043488029600)
1. [QuickRef.ME. Git cheatsheet.](https://quickref.me/git)
