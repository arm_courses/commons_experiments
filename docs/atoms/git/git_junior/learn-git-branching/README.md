# Learn Git Branching

## Basic Information

1. 学时：2-4
2. 目的：掌握`git`的基本使用
3. 内容与要求：通过[`Learn Git Branching`](https://learngitbranching.js.org)在线练习`git`
4. 环境条件：
    1. 软硬系统环境：`Desktop`(`macOS`或`Windows`或`Linux`)或`Server`(`Linux`)
    2. 应用软件环境：`browser`(推荐`chrome`或`firefox`或`safari`)

## Guides

1. 打开`Learn Git Branching`网址：<https://learngitbranching.js.org>
2. 按其中的指引练习`git`

![learn git branching](./.assets/image/learn-git-branching.png)

## Requirements of Report

![image-20240309142911256](./.assets/image/select_level.png)

1. 至少需完成`Main`中的各个关卡
2. 将每个关卡的练习结果截图保存到实验报告中

## References

1. [廖雪峰. Git教程.](https://www.liaoxuefeng.com/wiki/896043488029600)
1. [QuickRef.ME. Git cheatsheet.](https://quickref.me/git)
