# Git Exercises

## Basic Information

1. 学时：2-4
1. 目的：
    1. 掌握`git`的安装
    2. 掌握`git`的基本使用与进阶使用
2. 内容与要求：通过`git exercises`在本地安装`git`、练习和在线验证
3. 环境条件：
    1. 软硬系统环境：`Desktop`(`macOS`或`Windows`或`Linux`)或`Server`(`Linux`)
    2. 应用软件环境：`browser`(建议`chrome`/`firefox`/`safari`), `git`

## Guides

1. 打开`Git exercises`网址：<https://gitexercises.fracz.com>
2. 按其中的指引练习`git`

![git exercises](./.assets/image/git-exercises.png)

## Requirements of Report

1. 完成各个关卡
2. 将各个关卡的练习结果截图保存到实验报告中

## References

1. [廖雪峰. Git教程.](https://www.liaoxuefeng.com/wiki/896043488029600)
1. [QuickRef.ME. Git cheatsheet.](https://quickref.me/git)
